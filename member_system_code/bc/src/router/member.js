/* Layout */
import Layout from '../views/member/components/Layout'
import MobileLayout from '@/views/mobile/components/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/

export const memberRouterMap = [
  {
    path: '/login',
    component: () => import(`@/views/member/login/index`),
    hidden: true,
  },
  {
    path: '/404',
    name: '404',
    component: () => import(`@/views/404`),
    hidden: true,
  },
  {
    path: '/',
    name: '/',
    redirect: '/agreement',
  },
  {
    path: '/agreement',
    component: () => import(`@/views/member/agreement`),
    meta: { title: '用户协议', icon: 'form' },
  },
  {
    path: '/member',
    name: 'member',
    redirect: '/member/list',
    component: Layout,
    meta: { title: '首页', icon: 'form' },
    children: [
      {
        path: 'list',
        name: 'member_list',
        component: () => import(`@/views/member/list`),
        meta: { title: '游戏列表', icon: 'form' },
      },
      {
        path: 'profile',
        name: 'member_profile',
        component: () => import(`@/views/member/profile`),
        meta: { title: '用户资讯', icon: 'form' },
      },
      {
        path: 'raw',
        name: 'member_raw',
        component: () => import(`@/views/member/raw`),
        meta: { title: '未结明细', icon: 'form' },
      },
      {
        path: 'balanced',
        name: 'member_balanced',
        component: () => import(`@/views/member/balanced`),
        meta: { title: '今日已结', icon: 'form' },
      },
      {
        path: 'history/:type/:date',
        name: 'member_history',
        component: () => import(`@/views/member/history`),
        meta: { title: '两周报表', icon: 'form' },
      },
      {
        path: 'changepwd',
        component: () => import(`@/views/member/login/changepwd`),
        meta: { title: '修改密码', icon: 'form' },
      },
      {
        path: 'jiang',
        name: 'member_jiang',
        component: () => import(`@/views/member/jiang`),
        meta: { title: '开奖结果', icon: 'form' },
      },
      {
        path: 'rule',
        name: 'member_rule',
        component: () => import(`@/views/member/rule`),
        meta: { title: '游戏规则', icon: 'form' },
      },
      {
        path: 'option',
        name: 'member_option',
        component: () => import(`@/views/member/option`),
        meta: { title: '用户设置', icon: 'form' },
      },
    ],
  },

  {
    path: '/mobile',
    name: 'mobile',
    redirect: '/mobile/list',
    component: MobileLayout,
    children: [
      {
        path: 'list',
        name: 'mobile_list',
        component: () => import(`@/views/mobile/list`),
        meta: {
          title: '主页',
          icon: 'wap-home',
          bar: { index: 0, title: '投注' },
        },
      },
      {
        path: 'profile',
        name: 'mobile_profile',
        component: () => import(`@/views/mobile/profile`),
        meta: { title: '个人资讯', icon: 'contact' },
      },
      {
        path: 'raw',
        name: 'mobile_raw',
        component: () => import(`@/views/mobile/raw`),
        meta: {
          title: '未结明细',
          icon: 'records',
          bar: { index: 2, title: '未结' },
        },
      },
      {
        path: 'balanced',
        name: 'mobile_balanced',
        component: () => import(`@/views/mobile/balanced`),
        meta: {
          title: '今日已结',
          icon: 'completed',
          bar: { index: 3, title: '已结' },
        },
      },
      {
        path: 'history',
        name: 'mobile_history',
        component: () => import(`@/views/mobile/history`),
        meta: {
          title: '报表查询',
          icon: 'pending-orders',
          bar: { index: 4, title: '报表' },
        },
      },
      {
        path: 'jiang',
        name: 'mobile_jiang',
        component: () => import(`@/views/mobile/jiang`),
        meta: {
          title: '开奖结果',
          icon: 'idcard',
          bar: { index: 1, title: '开奖' },
        },
      },
      {
        path: 'rule',
        name: 'mobile_rule',
        component: () => import(`@/views/mobile/rule`),
        meta: { title: '规则', icon: 'question' },
      },
      {
        path: 'option',
        name: 'mobile_option',
        component: () => import(`@/views/mobile/option`),
        meta: { title: '用户设置', icon: 'setting' },
        hidden: true,
      },
      {
        path: 'changepwd',
        component: () => import(`@/views/mobile/changepwd`),
        meta: { title: '修改密码', icon: 'edit' },
        hidden: true,
      },
      {
        path: 'bet',
        component: () => import(`@/views/mobile/bet`),
        meta: { title: '投注', icon: 'after-sale' },
        hidden: true,
      },
    ],
  },
  // { path: '*', redirect: '/404', hidden: true },
]

export default memberRouterMap
