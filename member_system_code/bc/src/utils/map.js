// 字段参数统一映射
export const user = {
  // 类型
  type: {
    1: '代理',
    2: '会员',
    null: '代理和会员',
  },
  // 交易模式
  reset_mode: {
    1: '现金',
    2: '信用',
  },
  // 账号状态
  status: {
    1: '启用',
    2: '冻结',
    3: '停用',
  },
  // 盘口
  handicaps: {
    1: 'A',
    2: 'B',
    3: 'C',
    4: 'D',
  },
  // 剩余成数归属
  left_share_owner: {
    1: '全归庄家',
    2: '全归一级代理',
  },
  // 补货占成模式
  replenishment_share_mode: {
    1: '全归庄家',
    2: '全归一级代理',
    3: '按各级占成分配',
    4: '全归上级',
    5: '上线代理拦截',
  },
  // 赚取退水,注意要将key除以10
  earn_commission: [
    { 0: '全水退到底' },
    { 0.1: '赚取0.1%退水' },
    { 0.3: '赚取0.3%退水' },
    { 0.5: '赚取0.5%退水' },
    { 1: '赚取1.0%退水' },
    { 1.5: '赚取1.5%退水' },
    { 2: '赚取2.0%退水' },
    { 2.5: '赚取2.5%退水' },
    { 100: '赚取所有退水' },
  ],
  // 是否需要修改密码
  change_pwd: {
    0: '不需要',
    1: '需要',
  },
  // 子账号权限
  menus: {
    // '1': '下线管理',
    // '2': '即时注单',
    // '3': '自动补货',
    // '4': '交收分类报表',
    // '5': '后台更新日志',
    // '6': '额度转换记录报表',
    'realtime_order': '即时注单',
    'flow_order': '流水注单',
    'auto_padding': '自动补货',
    'sub_manage': '下线管理',
    'report_exchange': '交收报表查询',
    'backend_log': '后台更新日志',
  },
  // 游戏大类分类
  gameCatalogCode: {
    '快开彩': {
      '11': { name: '号码类(球号、车号、正号等)', className: 'haoma' },
      '12': {
        name: '两面类(大小、单双、龙虎、三军等)',
        className: 'liangmian',
      },
      '13': {
        name: '多项类(方位、中发白、共和过关等)',
        className: 'duoxiang',
      },
      '14': {
        name: '连码类(任选二、任选三、前二组选等)',
        className: 'lianma',
      },
      '15': { name: '其它(冠亚和、前中后三等)', className: 'qita' },
    },
    '香港彩': {
      '21': { name: '号码类', className: 'haoma' },
      '22': {
        name: '两面类',
        className: 'liangmian',
      },
      '23': {
        name: '多项类',
        className: 'duoxiang',
      },
      '24': {
        name: '连码类',
        className: 'lianma',
      },
      '25': { name: '其它', className: 'qita' },
    },
  },
  // 退水盘口设置
  commissionType: {
    '1': {
      field: 'a_commission',
      showName: 'A盘(%)',
      step: 0.0001,
      min: 0,
      max: 100,
    },
    '2': {
      field: 'b_commission',
      showName: 'B盘(%)',
      step: 0.0001,
      min: 0,
      max: 100,
    },
    '3': {
      field: 'c_commission',
      showName: 'C盘(%)',
      step: 0.0001,
      min: 0,
      max: 100,
    },
    '4': {
      field: 'd_commission',
      showName: 'D盘(%)',
      step: 0.0001,
      min: 0,
      max: 100,
    },
    '10': {
      field: 'order_max_amount',
      showName: '注单限额',
      step: 1,
      min: 0,
      max: 1000000,
    },
    // '11': { field: 'order_min_amount', showName: '注单最小额', step: 1, min: 0, max: 1000000 },
    '12': {
      field: 'period_max_amount',
      showName: '单期限额',
      step: 1,
      min: 0,
      max: 1000000,
    },
  },
  // 赔率设置
  oddsType: {
    '1': { field: 'odds', showName: '数值', step: 0.0001, min: 0, max: 100 },
  },
  // 赔率差设置
  oddsDiff: {
    // '1': { field: 'a_diff', showName: 'A盘(%)', step: 0.0001, min: -100, max: 100 },
    '2': {
      field: 'b_diff',
      showName: 'B盘(%)',
      step: 0.0001,
      min: -100,
      max: 100,
    },
    '3': {
      field: 'c_diff',
      showName: 'C盘(%)',
      step: 0.0001,
      min: -100,
      max: 100,
    },
    '4': {
      field: 'd_diff',
      showName: 'D盘(%)',
      step: 0.0001,
      min: -100,
      max: 100,
    },
  },
  // 按注降赔
  oddsReduceByOrder: {
    '1': {
      field: 'order_amount',
      showName: '数值',
      step: 1,
      min: 0,
      max: 100000,
    },
    '2': {
      field: 'reduce_unit',
      showName: '每次下调单位',
      step: 0.0001,
      min: 0,
      max: 1,
    },
    '3': {
      field: 'min_odds',
      showName: '最低赔率',
      step: 0.0001,
      min: 0,
      max: 1,
    },
    '4': { field: 'status', showName: '是否开启', step: 1, min: 0, max: 1 },
  },
  // 按期降赔
  oddsReduceByPeriod: {
    '1': { field: 'period', showName: '期数', step: 1, min: 0, max: 100000 },
    '2': {
      field: 'reduce_value1',
      showName: '两面没出降水',
      step: 0.0001,
      min: 0,
      max: 1,
    },
    '3': {
      field: 'reduce_value2',
      showName: '两面连出降水',
      step: 0.0001,
      min: 0,
      max: 1,
    },
    '4': {
      field: 'reduce_value3',
      showName: '号码遗漏降水',
      step: 0.0001,
      min: 0,
      max: 1,
    },
  },
  // 自动补货
  replenish: {
    '1': {
      field: 'amount',
      showName: '金额',
      step: 0.0001,
      min: 0,
      max: 1000000,
    },
    '2': { field: 'is_open', showName: '状态', step: 1, min: 0, max: 1 },
  },
  member: {
    username: '会员账号',
    handicaps: '盘口',
    account_status: '账户状态',
    account_mode: '账户类型',
    qk_account_amount: '快开信用额度',
    qk_remaining_amount: '快开可用额度',
    hk_account_amount: '香港信用额度',
    hk_remaining_amount: '香港可用额度',
  },
}

const map = {
  user: user,
}

export default map
