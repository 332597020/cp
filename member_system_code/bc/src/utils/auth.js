import Cookies from 'js-cookie'

const TokenKey = 'MemberBearer-Token'
const UserNameKey = 'MemberUserName'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, `Bearer ${token}`)
}

export function removeToken() {
  Cookies.remove(UserNameKey) // 用户名
  Cookies.remove('tabName') // 导航名
  Cookies.remove('tabNameChild') // 子导航名
  return Cookies.remove(TokenKey)
}

// 因为获取用户信息要通过用户名获取,所以将用户名保存到cookie
export function setUserName(name) {
  return Cookies.set(UserNameKey, name)
}

export function getUserName() {
  return Cookies.get(UserNameKey)
}


export function set(key, val) {
  return Cookies.set(key, val)
}

export function get(key) {
  return Cookies.get(key)
}

export function remove(key) {
  return Cookies.remove(key)
}
