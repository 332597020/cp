import Vue from 'vue'
import * as FastClick from 'fastclick'
import App from './App'
import router from './router'
import store from './store'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

// element ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN'
import '@/icons' // icon
import '@/permission' // permission control
import '@/styles/index.scss' // global css
Vue.use(ElementUI, { locale })

FastClick.attach(document.body)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
})
