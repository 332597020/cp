import { credit, selfLog } from '@/api/profile'

const profile = {
  state: {

  },

  mutations: {

  },

  actions: {
    // 获取个人信用信息
    GetCreditInfo({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        credit(payload).then(data => {
          console.log('获取个人信用信息参数', payload)
          console.log('获取个人信用信息', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取当前用户登陆日志
    GetSelfLog({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        selfLog(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

  },
}

export default profile
