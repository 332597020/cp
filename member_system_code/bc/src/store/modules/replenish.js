import { getReplenishList, setReplenishList } from '@/api/replenish'

// 自动补充模块
const replenish = {
  state: {

  },

  mutations: {

  },

  actions: {
    // 获取设置
    GetReplenishList({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getReplenishList(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 保存设置
    SetReplenishList({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setReplenishList(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

  },
}

export default replenish
