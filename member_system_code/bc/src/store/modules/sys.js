import {
  getSiteConfigs, setSiteConfigs,
  getBulletins, setBulletins,
  getGameSettings, setGameSettings,
  getDefaultOdds, setDefaultOdds,
  getOddsDiff, setOddsDiff,
  getDefaultCommissions, setDefaultCommissions,
  getOrderOddsReduce, setOrderOddsReduce,
  getPeriodOddsReduce, setPeriodOddsReduce,
  getDefaultHandicaps, getDefaultGameCategory,
  getGameConfig, saveGameConfig,
  getBaseOdds, saveBaseOdds,
  getOddsReduce, saveOddsReduce,
  getPaddingSettings, savePaddingSettings,
  getCommissions, saveCommissions,
  gameOptionss, systemSettings,
  updateSystemSettings, restoreCredit,
  getLimits, saveLimits,
  getOddsReduces, saveOddsReduces
} from '@/api/sys'

const sys = {
  state: {
    handicaps: null,
    category: null,
    gameOptions: [], // 彩种列表
  },

  mutations: {
    SET_HANDICAPS: (state, payload) => {
      state.handicaps = payload
    },
    SET_CATEGORY: (state, payload) => {
      state.category = payload
    },
    SET_GAMEOPTIONS: (state, payload) => {
      state.gameOptions = []
      for (const key in payload) {
        if (key !== 'message') {
          const temp = {
            label: payload[key],
            value: key,
          }
          state.gameOptions.push(temp)
        }
      }
    },
  },

  actions: {
    //
    GetSiteConfigs({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getSiteConfigs(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetSiteConfigs({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setSiteConfigs(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    GetBulletins({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getBulletins(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetBulletins({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setBulletins(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    GetGameSettings({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getGameSettings(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetGameSettings({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setGameSettings(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    GetDefaultOdds({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getDefaultOdds(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetDefaultOdds({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setDefaultOdds(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    GetOddsDiff({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getOddsDiff(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetOddsDiff({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setOddsDiff(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 默认退水
    GetDefaultCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getDefaultCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetDefaultCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setDefaultCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    GetOrderOddsReduce({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getOrderOddsReduce(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetOrderOddsReduce({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setOrderOddsReduce(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    GetPeriodOddsReduce({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getPeriodOddsReduce(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //
    SetPeriodOddsReduce({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setPeriodOddsReduce(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取默认盘口
    GetDefaultHandicaps({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getDefaultHandicaps(payload).then(data => {
          commit('SET_HANDICAPS', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取游戏分类
    GetDefaultGameCategory({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getDefaultGameCategory(payload).then(data => {
          commit('SET_CATEGORY', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取系统管理-参数
    GetGameConfig({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getGameConfig(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取系统管理-参数 保存参数
    SaveGameConfig({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        saveGameConfig(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 38、系统管理-基准赔率-获取基准赔率
    GetBaseOdds({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getBaseOdds(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 39、保存基准赔率
    SaveBaseOdds({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        saveBaseOdds(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 40、系统管理-自动降赔-获取自动降赔设置
    GetOddsReduce({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getOddsReduce(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 41、系统管理-自动降赔-保存自动降赔设置
    SaveOddsReduce({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        saveOddsReduce(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 42、获取系统补货设置
    GetPaddingSettings({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getPaddingSettings(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 43、保存系统补货设置
    SavePaddingSettings({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        savePaddingSettings(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 44、系统管理-退水-获取退水
    GetCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 45、系统管理-退水-保存退水
    SaveCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        saveCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 65、获取彩种列表
    GameOptionss({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        gameOptionss(payload).then(data => {
          commit('SET_GAMEOPTIONS', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 46、获取系统配置
    SystemSettings({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        systemSettings(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 47、保存系统设置
    UpdateSystemSettings({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        updateSystemSettings(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  48、恢复信用额度
    RestoreCredit({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        restoreCredit(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  51、系统管理-限额-获取限额数据
    GetLimits({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getLimits(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 52、系统管理-限额-保存限额数据
    SaveLimits({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        saveLimits(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 53、系统管理-降赔-获取降赔数据
    GetOddsReduces({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getOddsReduces(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 54、系统管理-降赔-保存降赔数据
    SaveOddsReduces({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        saveOddsReduces(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
  },
}

export default sys
