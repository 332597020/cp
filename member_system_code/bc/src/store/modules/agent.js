// 代理模块
import {
  validateAgent, createAgent, listUser, subCount, validateMember, createMember, changeStatus,
  validateUpdateAgent, updateAgent, validateUpdateMember, updateMember,
  listLoginHists,
  parentShare,
  withdrawAll, validateChangeAmount, changeAmount,
  getDatasCommissions, setCommissions,
  listSubAccount, validateAddSubAccount, addSubAccount, validateUpdateSubAccount, updateSubAccount, deleteSubAccount,
  hasOrder, getValidCode, getRealtimePt, getRealtimesInfo, getrRealtime, getNoticesList, deleteNotice, addNotice,
} from '@/api/agent'

const agent = {
  state: {
    ValidCode: null, // 验证码
    nowSelectedGame: {}, // 当前选中的游戏
    realtime: '', // 即时注单
    realtimeTitle: '', // 即时注单返回的数据的子标题
    realtimePt: '', // 即时注单导航信息
    realTimeMode: 0, // 即时注单显示模式
    realTimeMultiLine: false, // 是否多行显示
  },

  mutations: {
    // 验证码
    AGENT_GET_VALID_CODE: (state, payload) => {
      state.ValidCode = payload
    },
    SET_NOW_SELECTED_GAME: (state, payload) => {
      state.nowSelectedGame = payload
    },
    SET_REALTIME: (state, payload) => {
      state.realtime = []
      state.realtimeTitle = []
      const temp = []
      const tempTitle = []
      let halfLength = 0
      for (const key in payload) {
        if (key !== 'message') {
          temp.push(payload[key])
          tempTitle.push(key)
        }
      }
      console.log('state.realtimeTitle.length', temp)
      state.realTimeMode = parseInt(temp[0][0].mode)
      if (state.realTimeMode === 1) {
        console.log('state.realtimeTitle.length', tempTitle.length)
        if (tempTitle.length > 5) {
          state.realTimeMultiLine = true
          halfLength = parseInt(tempTitle.length / 2)
          if (tempTitle.length === 6) {
            halfLength = 2
          }
          if (halfLength > 4) {
            halfLength = 4
          }
          let tempRealTime = []
          let tempRealTimeTitle = []
          for (const key in temp) {
            tempRealTimeTitle.push(tempTitle[key])
            tempRealTime.push(temp[key])
            if (tempRealTime.length === halfLength) {
              state.realtime.push(tempRealTime)
              state.realtimeTitle.push(tempRealTimeTitle)
              tempRealTime = []
              tempRealTimeTitle = []
            }
          }
        } else {
          state.realTimeMultiLine = false
          state.realtime = temp
          state.realtimeTitle = tempTitle
        }
      } else {
        state.realTimeMultiLine = false
        state.realtime = temp
        state.realtimeTitle = tempTitle
      }
      console.log('11111', state.realtime)
      console.log('11111', state.realtimeTitle)
      console.log('11111', payload)
      console.log('state.realTimeMode', state.realTimeMode)
    },
    SET_REALTIMEPT: (state, payload) => {
      state.realtimePt = payload
    },
  },

  actions: {
    // 下级代理列表
    async ListAgent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        listUser(payload.params, payload.pagination.currentPage).then(data => {
          console.log('下级代理列表参数', payload.pagination.currentPage)
          console.log('下级代理列表返回值', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 下级数量统计
    async SubCount({ commit }, payload) {
      return new Promise((resolve, reject) => {
        subCount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证代理
    async ValidateAgent({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 创建代理
    async CreateAgent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        createAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证会员
    async ValidateMember({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 创建会员
    async CreateMember({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        createMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新用户信息
    // 验证代理更新
    async ValidateUpdateAgent({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateUpdateAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新代理
    async UpdateAgent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        updateAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证更新会员
    async ValidateUpdateMember({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateUpdateMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新会员
    async UpdateMember({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        updateMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新用户状态
    async ChangeStatus({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        changeStatus(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 用户登录日志
    async ListLoginHists({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        listLoginHists(payload.params).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取上级占成
    async ParentShare({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        parentShare(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 提取全部额度
    async WithdrawAll({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        withdrawAll(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证额度变更
    async ValidateChangeAmount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        validateChangeAmount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 变更额度
    async ChangeAmount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        changeAmount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取上级commissions
    async GetDatasCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getDatasCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 设置commissions
    async SetCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 子账号列表
    async ListSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        listSubAccount(payload.params, payload.pagination.currentPage).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证新增子账号
    async ValidateAddSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        validateAddSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 新增子账号
    async  AddSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        addSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证更新子账号
    async ValidateUpdateSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        validateUpdateSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证更新子账号
    async UpdateSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        updateSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 删除子账号
    async DeleteSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        deleteSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 有订单
    async  HasOrder({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        hasOrder(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取验证码
    async ValidCode({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getValidCode().then(data => {
          console.log('获取验证码', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    //  55、即时注单
    async GetRealtime({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getrRealtime(payload).then(data => {
          commit('SET_REALTIME', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  55-1、即时注单导航菜单
    async GetRealtimePt({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getRealtimePt(payload).then(data => {
          commit('SET_REALTIMEPT', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  55-2、即时注单-最新一期信息
    async GetRealtimesInfo({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getRealtimesInfo(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 63、站内消息分页列表
    async GetNoticesList({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getNoticesList(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 64-2、删除站内消息（公告）
    async DeleteNotice({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        deleteNotice(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  64、添加站内消息（公告）
    async AddNotice({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        addNotice(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
  },
}

export default agent
