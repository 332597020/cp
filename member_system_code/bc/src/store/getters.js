const getters = {
  sidebar: state => state.app.sidebar,
  info: state => state.user.info,
  menus: state => state.user.menus, // 头部导航菜单
  games: state => state.game.games, // 全部游戏
  handicaps: state => state.sys.handicaps, // 系统默认盘口
  category: state => state.sys.category, // 系统游戏分类目录
  gameOptions: state => state.sys.gameOptions, // 彩种列// 会员state
  memberIsMobile: state => state.member.isMobile, // 是否手机界面
  memberInfo: state => state.member.info, // 会员信息
  memberGames: state => state.member.games, // 获取当前会员可投注的彩种列表
  gamesall: state => state.member.gamesall, // 获取所有彩种列表
  memberRule: state => state.member.rule, // 游戏规则
  memberGameSelected: state => state.member.gameSelected, // 获取导航栏
  memberGamePeriod: state => state.member.gamePeriod, // 获取当前期数信息
  memberGameAccount: state => state.member.gameAccount, // 账户信息,资金等
  memberPages: state => state.member.pages, // 获取页面类型分类两面等等
  memberPageSelected: state => state.member.pageSelected, // 获取页面类型分类选定分页两面等等
  memberLottery: state => state.member.lottery, // 获取下注
  memberOdds: state => state.member.odds, // 获取当前游戏赔率
  memberBets: state => state.member.bets, // 选定的投注项目
  memberRaw: state => state.member.raw, // 获取未结明细
  memberBalanced: state => state.member.balanced, // 今日已结
  memberHistory: state => state.member.history, // 获取导航栏
  memberListActivePanel: state => state.member.listaAtivePane,
  memberLastResult(state){
    return state.member.lastGameResult
  }, // 头部最新开奖期数与结果
  memberLastOrder: state => state.member.lastGameOrder, // 获取最新注单结果
  memberresult: state => state.member.Lotteryss, // 获取最新开奖结果
  noticegg: state => state.member.notices, // 公告滚动
  noticeggadd: state => state.member.noticesadd, // 全部公告
  memberFirstBet(state) {
    console.log('-----------', state.member.firstBets)
    return state.member.firstBets
  }, // 获取第一次投注时返回的投注信息
  nowSelectedGame: state => state.agent.nowSelectedGame, // 获取代理系统中当前选中的游戏
  realtime: state => state.agent.realtime, // 获取即时注单
  realtimeTitle: state => state.agent.realtimeTitle, // 获取即时注单的子标题
  realtimePt: state => state.agent.realtimePt, // 即时注单导航信息
  realTimeMode: state => state.agent.realTimeMode, // 即时注单显示模式
  realTimeMultiLine: state => state.agent.realTimeMultiLine, // 是否多行显示
  diffCloseVuex: state => state.member.diffCloseVuex,
  faletk: state => state.member.faletk,
  skin: state => state.member.skin,
  gamePeriodBalls: state => state.member.gamePeriodBalls,
}
export default getters
