import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import agent from './modules/agent'
import game from './modules/game'
import profile from './modules/profile'
import replenish from './modules/replenish'
import sys from './modules/sys'
import member from './modules/member'
import getters from './getters'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    app,
    user,
    agent,
    game,
    profile,
    replenish,
    sys,
    member,
  },
  getters,
  plugins: [createPersistedState({
    storage: window.sessionStorage,
  })],
})

export default store
