// 用户登录相关接口
import request from '@/utils/request'

// 1登陆
export function login(params) {
  return request({
    url: '/users/login',
    method: 'post',
    data: params,
  })
}

// 10个人信息
export function usersInfo(params) {
  return request({
    url: '/users/info',
    method: 'post',
    data: params,
  })
}

// 2修改密码
export function changePwd(params) {
  return request({
    url: '/users/change-pwd',
    method: 'post',
    data: params,
  })
}

// 13、获取当前会员可投注的彩种列表
export function getGames(params) {
  return request({
    url: '/games/get-all-games',
    method: 'post',
    data: params,
  })
}

// 3获取指定彩种的页面类型
export function ordersPages(params) {
  return request({
    url: '/orders/pages',
    method: 'post',
    data: params,
  })
}

// 4获取下注
// export function ordersLottery(params) {
//   return request({
//     url: '/orders/lottery',
//     method: 'post',
//     data: params,
//   })
// }

// 5获取游戏赔率
export function ordersOdds(params) {
  return request({
    url: '/orders/odds',
    method: 'post',
    data: params,
  })
}

// 6会员提交
export function bet(params) {
  // 注意地址前缀不同
  // const url = 'http://hfdz621.tpddns.cn:8081'
  return request({
    url: '/orders/bet',
    method: 'post',
    data: params,
  })
}

// 7未结明细
export function ordersRaw(params) {
  return request({
    url: `/orders/raw/${params.page}`,
    method: 'post',
    data: params,
  })
}

// 8今日已结
export function ordersBalanced(params) {
  return request({
    url: `/orders/balanced/${params.page}`,
    method: 'post',
    data: params,
  })
}

// 9周报表
export function ordersHistory(params) {
  return request({
    url: '/orders/history',
    method: 'post',
    data: params,
  })
}
// 10、两周报表详情
export function historyDetails(params) {
  return request({
    url: '/orders/history-details',
    method: 'post',
    data: params,
  })
}

// 11 游戏规则
export function gameRule(params) {
  return request({
    url: '/lotteries/rule',
    method: 'post',
    data: params,
  })
}

// 14 获取当前期数信息
export function period(params) {
  return request({
    url: '/orders/refresh-period',
    method: 'post',
    data: params,
  })
}

// 15 获取开奖结果
export function result(params) {
  return request({
    url: '/lotteries/result',
    method: 'post',
    data: params,
  })
}

// 16 获取验证码
export function validCode(params) {
  return request({
    url: '/users/gen-valid-code',
    method: 'get',
    data: params,
  })
}

// 17 是否需要修改密码
export function needChangepwd(params) {
  return request({
    url: '/users/need-change-pwd',
    method: 'post',
    data: params,
  })
}

// 18 账户信息,资金等
export function gameAccount(params) {
  return request({
    url: '/users/account',
    method: 'post',
    data: params,
  })
}

// 19 头部最新开奖期数与结果
export function lastResult(params) {
  return request({
    url: '/orders/last-result',
    method: 'post',
    data: params,
  })
}

// 20 最新注单
export function lastOrder(params) {
  return request({
    url: '/orders/new',
    method: 'post',
    data: params,
  })
}

// 21、获取所有彩种项
export function getall(params) {
  return request({
    url: '/lotteries/game-options',
    method: 'post',
    data: params,
  })
}

// 22、公告滚动
export function notice(params) {
  return request({
    url: '/notices/top',
    method: 'post',
    data: params,
  })
}

// 22、全部公告
export function addnotice(params) {
  return request({
    url: '/notices/all',
    method: 'post',
    data: params,
  })
}
