// 游戏相关
import request from '@/utils/request'

// 全部游戏列表
export function getAllGames(params) {
  return request({
    url: '/games/get-all-games',
    method: 'post',
    data: params,
  })
}
// 65.获取彩种列表
export function getGameOptions(params) {
  return request({
    url: '/games/game-options',
    method: 'post',
    data: params,
  })
}
// /lotteries/game-options
// 21.获取彩种列表
export function getLotteriesGameOptions(params) {
  return request({
    url: '/lotteries/game-options',
    method: 'post',
    data: params,
  })
}
