// 个人信息
import request from '@/utils/request'

// 用户信用信息
export function credit(params) {
  return request({
    url: '/agents/credit-info',
    method: 'post',
    data: params,
  })
}

// 用户登陆日志
export function selfLog(params) {
  return request({
    url: '/loginhists/self-log',
    method: 'post',
    data: params,
  })
}
