import request from '@/utils/request'

// 自动补充相关接口
// 获取当前用户自动补充设置列表
export function getReplenishList(params) {
  return request({
    url: '/replenishment-settings/get-setting',
    method: 'post',
    params,
  })
}

// 设置当前用户自动补充
export function setReplenishList(params) {
  return request({
    url: '/replenishment-settings/save-setting',
    method: 'post',
    params,
  })
}
