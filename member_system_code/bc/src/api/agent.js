// agent相关接口
import request from '@/utils/request'

// user list
export function listUser(params, page = 1) {
  return request({
    url: `/users/list/${page}`,
    method: 'post',
    data: params,
  })
}

// user 下级数量统计
export function subCount(params) {
  return request({
    url: '/users/sub-count',
    method: 'post',
    data: params,
  })
}

// 验证agent表单
export function validateAgent(params) {
  return request({
    url: '/users/validate-agent-form',
    method: 'post',
    data: params,
  })
}

// 创建新agent
export function createAgent(params) {
  return request({
    url: '/users/create-agent',
    method: 'post',
    data: params,
  })
}

// 验证member表单
export function validateMember(params) {
  return request({
    url: '/users/validate-member-form',
    method: 'post',
    data: params,
  })
}

// 创建新member
export function createMember(params) {
  return request({
    url: '/users/create-member',
    method: 'post',
    data: params,
  })
}

// 编辑用户信息
// 验证agent更新
export function validateUpdateAgent(params) {
  return request({
    url: '/users/validate-update-agent-form',
    method: 'post',
    data: params,
  })
}

// 更新新agent
export function updateAgent(params) {
  return request({
    url: '/users/update-agent',
    method: 'post',
    data: params,
  })
}

// 验证member更新
export function validateUpdateMember(params) {
  return request({
    url: '/users/validate-member-update-form',
    method: 'post',
    data: params,
  })
}

// 更新新member
export function updateMember(params) {
  return request({
    url: '/users/update-member',
    method: 'post',
    data: params,
  })
}

// 更新用户状态
export function changeStatus(params) {
  return request({
    url: '/users/change-status',
    method: 'post',
    data: params,
  })
}

// 用户登录日志
export function listLoginHists(params) {
  return request({
    url: '/loginhists/list',
    method: 'post',
    data: params,
  })
}

// 获取上级占成
export function parentShare(params) {
  return request({
    // url: '/shares/get-upper-agents-share',
    url: '/shares/upper-shares',
    method: 'post',
    data: params,
  })
}

// 提取用户全部额度
export function withdrawAll(params) {
  return request({
    url: '/users/withdraw-all',
    method: 'post',
    data: params,
  })
}

// 验证用户额度
export function validateChangeAmount(params) {
  return request({
    url: '/users/validate-change-amount-form',
    method: 'post',
    data: params,
  })
}

// 变更用户额度
export function changeAmount(params) {
  return request({
    url: '/users/change-amount',
    method: 'post',
    data: params,
  })
}

// 获取上级commissions
export function getDatasCommissions(params) {
  return request({
    url: '/commissions/datas',
    method: 'post',
    data: params,
  })
}

// 保存用户commissions
export function setCommissions(params) {
  return request({
    url: '/commissions/save',
    method: 'post',
    data: params,
  })
}

// 子账号列表
export function listSubAccount(params, page = 1) {
  return request({
    url: `/users/subaccount-list/${page}`,
    method: 'post',
    data: params,
  })
}

// 验证新增子账号
export function validateAddSubAccount(params) {
  return request({
    url: `/users/validate-subaccount-form`,
    method: 'post',
    data: params,
  })
}

// 新增子账号
export function addSubAccount(params) {
  return request({
    url: `/users/create-sub-account`,
    method: 'post',
    data: params,
  })
}

// 验证更新子账号
export function validateUpdateSubAccount(params) {
  return request({
    url: `/users/validate-update-subaccount-form`,
    method: 'post',
    data: params,
  })
}

// 更新子账号
export function updateSubAccount(params) {
  return request({
    url: `/users/update-subaccount`,
    method: 'post',
    data: params,
  })
}

// 删除子账号
export function deleteSubAccount(params) {
  return request({
    url: `/users/delete-subaccount`,
    method: 'post',
    data: params,
  })
}

// 56是否存在交易行为
export function hasOrder(params) {
  return request({
    url: `/users/has-order`,
    method: 'post',
    data: params,
  })
}
// 23生成验证码
export function getValidCode() {
  return request({
    url: `/sites/gen-valid-code`,
    method: 'post',
  })
}
// 55、获取即时注单
export function getrRealtime(params) {
  return request({
    url: `/orders/realtime`,
    method: 'post',
    data: params,
  })
}
// 55-1、即时注单导航菜单
export function getRealtimePt(params) {
  return request({
    url: `/orders/realtime-pt`,
    method: 'post',
    data: params,
  })
}
// 55-2、即时注单-最新一期信息
export function getRealtimesInfo(params) {
  return request({
    url: `/realtimes/info`,
    method: 'post',
    data: params,
  })
}

// 63、站内消息分页列表
export function getNoticesList(params) {
  if (params) {
    return request({
      url: `/notices/list/` + params,
      method: 'post',
      data: params,
    })
  } else {
    return request({
      url: `/notices/list`,
      method: 'post',
      data: params,
    })
  }
}

// 64-2、删除站内消息（公告）
export function deleteNotice(params) {
  return request({
    url: `/notices/unlink`,
    method: 'post',
    data: params,
  })
}

// 64、添加站内消息（公告）
export function addNotice(params) {
  return request({
    url: `/notices/add`,
    method: 'post',
    data: params,
  })
}
