// 个人信息
import request from '@/utils/request'

// 用户信用信息
export function credit(params) {
  return request({
    url: '/agents/credit-info',
    method: 'post',
    data: params,
  })
}

