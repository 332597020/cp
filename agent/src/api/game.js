// 游戏相关
import request from '@/utils/request'

// 全部游戏列表
export function getAllGames(params) {
  return request({
    url: '/games/get-all-games',
    method: 'post',
    data: params,
  })
}
// 65.获取彩种列表
export function getGameOptions(params) {
  return request({
    url: '/games/game-options',
    method: 'post',
    data: params,
  })
}

// 65.获取开奖结果
export function getResult(params) {
  return request({
    url: '/games/result',
    method: 'post',
    data: params,
  })
}

