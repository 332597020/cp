import router from './router'
import store from './store'
import NProgress from 'nprogress' // Progress 进度条
import 'nprogress/nprogress.css' // Progress 进度条样式
 import { Message } from 'element-ui'
import { setToken, getToken, getUserName, removeToken, setAlreadyLogin, setUserName } from '@/utils/auth' // 验权

const whiteList = ['/login'] // 不重定向白名单
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (sessionStorage.token) {
    // 有token 曾经登录
    if (to.path === '/login') {
      next()
    } else if (to.path === '/changepwd') {
      next()
    } else {
      // 默认代理系统
      let infoDispatch = 'GetUserInfo'
      let userInfoLength = Object.keys(store.getters.info).length
      console.log('getToken()值', getToken())
      if (process.env.BASE_API === '/memberapi') {
        // 会员系统
        infoDispatch = 'MemberGetInfo'
        userInfoLength = Object.keys(store.getters.memberInfo).length
      }
      // console.log('userInfoLength是什么', userInfoLength)
      // 注意对info为空的判断
      if (userInfoLength === 0 && sessionStorage.token) {
        let userName = getUserName()
        if (!userName || sessionStorage.userName != getUserName()) {
          userName = JSON.parse(sessionStorage.userName)
        }
        // 拉取用户信息,成功后保存store
        store
          .dispatch(infoDispatch, { username: userName, set: true })
          .then(res => {
            sessionStorage.level = JSON.stringify(res.level)
            sessionStorage.name = JSON.stringify(res.username)
            if (!localStorage.level) {
              setToken(sessionStorage.token)
              setUserName(res.username)
              localStorage.level = JSON.stringify(res.level)
              localStorage.name = JSON.stringify(res.username)
              sessionStorage.comLevel = JSON.stringify(res.level)
              sessionStorage.comName = JSON.stringify(res.username)
              next()
            } else {
              if (parseInt(JSON.parse(localStorage.level)) >= parseInt(res.level)) {
                localStorage.level = JSON.stringify(res.level)
                localStorage.name = JSON.stringify(res.username)
                sessionStorage.comLevel = JSON.stringify(res.level)
                sessionStorage.comName = JSON.stringify(res.username)
                setUserName(res.username)
                setToken(sessionStorage.token)
                next()
              } else {
                store.dispatch('FedLogOut').then(() => {
                  alert('同一浏览器只能登录一个账号，请退出上一个账号再登录')
                  next({ path: '/login' })
                  NProgress.done()
                })
              }
            }
          })
          .catch((res) => {
            console.log(res)
            store.dispatch('FedLogOut').then(() => {
              Message.error('验证失败,请重新登录')
              next({ path: '/login' })
            })
          })
      } else {
        next()
      }
    }
  } else {
    // removeToken() // 删除cookie信息
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next('/login')
    }
  }
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
