import Cookies from 'js-cookie'

const app = {
  state: {
    sidebar: {
      opened: !+Cookies.get('sidebarStatus'),
    },
    cookies: null,
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
    },
  },
  actions: {
    ToggleSideBar: ({ commit }) => {
      commit('TOGGLE_SIDEBAR')
    },
    // 设置cookie
    SetCookie({ commit, state }, payload) {
      return Cookies.set(payload[0], payload[1])
    },
    // 获取cookie
    GetCookie({ commit, state }, payload) {
      return Cookies.get(payload)
    },
  },
}

export default app
