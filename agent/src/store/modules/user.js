import {
  login,
  getUserInfo,
  needChangePwd,
  changePwd,
  fetchMenus, onlineStat,
} from '@/api/login'
import { setToken, removeToken, setUserName } from '@/utils/auth'

const user = {
  state: {
    info: {}, // 用户详细信息
    menus: null, // 导航主菜单,根据不同用户权限,不同
  },

  mutations: {
    SET_INFO: (state, payload) => {
      state.info = payload
    },
    SET_MENUS: (state, payload) => {
      state.menus = payload
    },
  },

  actions: {
    // 登录
    async Login({ commit }, payload) {
      return new Promise((resolve, reject) => {
        login(payload)
          .then(data => {
            if (typeof data.token === 'string') {
              // setToken(data.token)
              // setUserName(payload.username)
            }
            resolve(data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    // 获取用户信息
    async GetUserInfo({ commit, state }, payload) {
      if (payload.username === state.info.username) {
        // 如果是全局账号,不重复请求数据
        return state.info
      }
      return new Promise((resolve, reject) => {
        getUserInfo({ username: payload.username })
          .then(data => {
            if (payload.set) {
              // 保存
              commit('SET_INFO', data)
            }
            resolve(data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    // 检验是否需要修改密码
    async NeedChangePwd({ commit }, payload) {
      return new Promise((resolve, reject) => {
        needChangePwd(payload)
          .then(data => {
            resolve(data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    // 修改密码
    async ChangePwd({ commit }, payload) {
      return new Promise((resolve, reject) => {
        changePwd(payload)
          .then(data => {
            resolve(data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    // 登出
    // LogOut({ commit, state }) {
    //   return new Promise((resolve, reject) => {
    //     logout(state.name).then(() => {
    //       commit('SET_TOKEN', '')
    //       commit('SET_USER_TYPE', '')
    //       removeToken()
    //       resolve()
    //     }).catch(error => {
    //       reject(error)
    //     })
    //   })
    // },

    // 前端 登出
    async FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_INFO', '')
        removeToken()
        resolve()
      })
    },

    // 获取主菜单
    async FetchMenus({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        fetchMenus({})
          .then(data => {
            console.log('获取主菜单', data)
            commit('SET_MENUS', data)
            resolve(data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    // 66、统计在线会员人数（页面右上角处）
    async OnlineStat({ commit }, payload) {
      return new Promise((resolve, reject) => {
        onlineStat(payload)
          .then(data => {
            resolve(data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

  },
}

export default user
