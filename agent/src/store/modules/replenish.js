import {
  getReplenishList, setReplenishList,
  getReplenishmentsGames, getLogs,
} from '@/api/replenish'

// 自动补充模块
const replenish = {
  state: {

  },

  mutations: {

  },

  actions: {
    // 获取设置
    GetReplenishList({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getReplenishList(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 保存设置
    SetReplenishList({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setReplenishList(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 保存设置
    GetReplenishmentsGames({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getReplenishmentsGames(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取自动补货变更日志
    GetLogs({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getLogs(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

  },
}

export default replenish
