// 代理模块
import {
  validateAgent, createAgent, listUser, subCount, validateMember, createMember, changeStatus,
  validateUpdateAgent, updateAgent, validateUpdateMember, updateMember,
  listLoginHists,
  parentShare,
  withdrawAll, validateChangeAmount, changeAmount,
  getDatasCommissions, setCommissions,
  listSubAccount, validateAddSubAccount, addSubAccount, validateUpdateSubAccount, updateSubAccount, deleteSubAccount,
  hasOrder, getValidCode, getRealtimePt, getRealtimesInfo, getrRealtime, getNoticesList, deleteNotice, addNotice,
  getMbets, setPadding, getSelfLog, getGameReport, getReport, getOrderBets, getGamePeriods, changeSubaccStatus,
  getSubAccInfo, getLogsList, getShareConditions, getUseTfa, getGenTfa, getSaveTfacode, getResetTfa, getOddsdatas, saveOdds,
} from '@/api/agent'

const agent = {
  state: {
    ValidCode: null, // 验证码
    nowSelectedGame: {}, // 当前选中的游戏
    realtime: '', // 即时注单
    realtimeTitle: '', // 即时注单返回的数据的子标题
    realtimePt: '', // 即时注单导航信息
    realTimeMode: 0, // 即时注单显示模式
    realTimeMultiLine: false, // 是否多行显示
    sortForm: '', // 分类表单
    payFrom: '', // 交收表单
    payFromParams: '', // 交收表单参数
    noteDetails: '', // 注单详情
    otherUserName: '', // 子代理或者会员的名字
    appMainTop: '',
  },

  mutations: {
    SET_APPMAINTOP: (state, payload) => {
      state.appMainTop = payload
    },
    SET_OTHERUSERNAME: (state, payload) => {
      state.otherUserName = payload
    },
    SET_NOTEDETAILS: (state, paload) => {
      state.noteDetails = paload
    },
    SET_PAYFROMPARAMS: (state, payload) => {
      state.payFromParams = payload
    },
    SET_SORTFROM: (state, payload) => {
      state.sortForm = JSON.parse(JSON.stringify(payload))
    },
    SET_PAYFROM: (state, payload) => {
      state.payFrom = JSON.parse(JSON.stringify(payload))
    },
    // 验证码
    AGENT_GET_VALID_CODE: (state, payload) => {
      state.ValidCode = payload
    },
    SET_NOW_SELECTED_GAME: (state, payload) => {
      state.nowSelectedGame = payload
    },
    SET_REALTIME: (state, payload) => {
      state.realtime = []
      state.realtimeTitle = []
      const temp = []
      const tempTitle = []
      let halfLength = 0
      for (const key in payload) {
        if (key !== 'message') {
          temp.push(payload[key])
          tempTitle.push(key)
        }
      }
      state.realTimeMode = parseInt(temp[0][0].mode)
      if (state.realTimeMode === 1) {
        if (tempTitle.length > 5) {
          state.realTimeMultiLine = true
          halfLength = parseInt(tempTitle.length / 2)
          if (tempTitle.length === 6) {
            halfLength = 2
          }
          if (halfLength > 4) {
            halfLength = 4
          }
          let tempRealTime = []
          let tempRealTimeTitle = []
          for (const key in temp) {
            tempRealTimeTitle.push(tempTitle[key])
            tempRealTime.push(temp[key])
            if (tempRealTime.length === halfLength) {
              state.realtime.push(tempRealTime)
              state.realtimeTitle.push(tempRealTimeTitle)
              tempRealTime = []
              tempRealTimeTitle = []
            }
          }
        } else {
          state.realTimeMultiLine = false
          state.realtime = temp
          state.realtimeTitle = tempTitle
        }
      } else {
        state.realTimeMultiLine = false
        state.realtime = temp
        state.realtimeTitle = tempTitle
      }
    },
    SET_REALTIMEPT: (state, payload) => {
      state.realtimePt = payload
    },
  },

  actions: {
    // 下级代理列表
    async ListAgent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        listUser(payload.params, payload.pagination.currentPage).then(data => {
          console.log('下级代理列表参数', payload.pagination.currentPage)
          console.log('下级代理列表返回值', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 下级数量统计
    async SubCount({ commit }, payload) {
      return new Promise((resolve, reject) => {
        subCount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证代理
    async ValidateAgent({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 创建代理
    async CreateAgent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        createAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证会员
    async ValidateMember({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 创建会员
    async CreateMember({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        createMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新用户信息
    // 验证代理更新
    async ValidateUpdateAgent({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateUpdateAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新代理
    async UpdateAgent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        updateAgent(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证更新会员
    async ValidateUpdateMember({ commit }, payload) {
      return new Promise((resolve, reject) => {
        validateUpdateMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新会员
    async UpdateMember({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        updateMember(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新用户状态
    async ChangeStatus({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        changeStatus(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 用户登录日志
    async ListLoginHists({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        listLoginHists(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取上级占成
    async ParentShare({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        parentShare(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 提取全部额度
    async WithdrawAll({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        withdrawAll(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证额度变更
    async ValidateChangeAmount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        validateChangeAmount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 变更额度
    async ChangeAmount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        changeAmount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取上级commissions
    async GetDatasCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getDatasCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 设置commissions
    async SetCommissions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setCommissions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 子账号列表
    async ListSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        listSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证新增子账号
    async ValidateAddSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        validateAddSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 新增子账号
    async  AddSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        addSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 验证更新子账号
    async ValidateUpdateSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        validateUpdateSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 更新子账号
    async UpdateSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        updateSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 删除子账号
    async DeleteSubAccount({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        deleteSubAccount(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 有订单
    async  HasOrder({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        hasOrder(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取验证码
    async ValidCode({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getValidCode().then(data => {
          console.log('获取验证码', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    //  55、即时注单
    async GetRealtime({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getrRealtime(payload).then(data => {
          commit('SET_REALTIME', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  55-1、即时注单导航菜单
    async GetRealtimePt({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getRealtimePt(payload).then(data => {
          commit('SET_REALTIMEPT', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  55-2、即时注单-最新一期信息
    async GetRealtimesInfo({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getRealtimesInfo(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 63、站内消息分页列表
    async GetNoticesList({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getNoticesList(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 64-2、删除站内消息（公告）
    async DeleteNotice({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        deleteNotice(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  64、添加站内消息（公告）
    async AddNotice({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        addNotice(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  64、类别2的即时注单弹框列表 代理系统
    async GetMbets({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getMbets(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 39、即时注单页面手动补货
    async SetPadding({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        setPadding(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 26、获取当前登录代理的登录记录列表
    async GetSelfLog({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getSelfLog(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 41、 报表查询 分类报表
    async GetGameReport({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getGameReport(payload).then(data => {
          commit('SET_SORTFROM', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 42、报表查询 交收报表
    async GetReport({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getReport(payload).then(data => {
          commit('SET_PAYFROM', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 43、注单明细
    async GetOrderBets({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getOrderBets(payload).then(data => {
          commit('SET_NOTEDETAILS', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    //  40、获取单一彩种期数
    async GetGamePeriods({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getGamePeriods(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 30-1、修改子账号状态
    async ChangeSubaccStatus({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        changeSubaccStatus(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    //  30-2、修改子账号状态
    async GetSubAccInfo({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getSubAccInfo(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户管理的记录
    async GetLogsList({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getLogsList(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取创建子代理时的占成条件
    async getShareConditions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getShareConditions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 49、返回当前代理是否绑定了二次验证
    async GetUseTfa({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getUseTfa(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 50、生成绑定二次验证码链接二维码
    async GetGenTfa({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getGenTfa(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 51、绑定二次验证
    async GetSaveTfacode({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getSaveTfacode(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 52、解绑二次验证
    async GetResetTfa({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getResetTfa(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //  24、获取编辑用户赔率差时所需的自身以及直属父级赔率差设置
    async GetOddsdatas({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getOddsdatas(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 25、保存用户（代理或者会员）赔率差
    async SaveOdds({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        saveOdds(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
  },
}

export default agent
