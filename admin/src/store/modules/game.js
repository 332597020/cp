import {
  getAllGames,
  getGameOptions,
  getResult,
} from '@/api/game'

const game = {
  state: {
    games: null, // 全部游戏
  },

  mutations: {
    SET_GAMES: (state, payload) => {
      state.games = payload
    },

  },

  actions: {
    // 获取全部游戏
    GetAllGames({ commit, state }, payload) {
      if (state.games) {
        // 不重复请求数据
        return state.games
      }
      return new Promise((resolve, reject) => {
        getAllGames(payload).then(data => {
          commit('SET_GAMES', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取彩种列表
    GetGameOptions({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getGameOptions(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取开奖结果
    GetResult({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        getResult(payload).then(data => {
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
  },
}

export default game
