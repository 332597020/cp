import request from '@/utils/request'

// 自动补充相关接口
// 获取当前用户自动补充设置列表
export function getReplenishList(params) {
  return request({
    url: '/replenishments/get-setting',
    method: 'post',
    data: params,
  })
}

// 设置当前用户自动补充
export function setReplenishList(params) {
  return request({
    url: '/replenishments/save-setting',
    method: 'post',
    data: params,
  })
}
// 获取自动补充的彩种导航
export function getReplenishmentsGames(params) {
  return request({
    url: '/replenishments/games',
    method: 'post',
    data: params,
  })
}

// 获取自动补充的彩种导航
export function getLogs(params) {
  return request({
    url: '/replenishments/logs',
    method: 'post',
    data: params,
  })
}
