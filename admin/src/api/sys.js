// 用户登录相关接口
import request from '@/utils/request'

// 40获取游戏默认比例
export function getDefaultOdds(params) {
  return request({
    url: 'systems/get-default-odds',
    method: 'post',
    data: params,
  })
}

// 41保存游戏默认比例
export function setDefaultOdds(params) {
  return request({
    url: 'systems/save-default-odds',
    method: 'post',
    data: params,
  })
}

// 42获取游戏比例差
export function getOddsDiff(params) {
  return request({
    url: 'systems/get-odds-diff',
    method: 'post',
    data: params,
  })
}

// 43获取游戏比例差
export function setOddsDiff(params) {
  return request({
    url: 'systems/save-odds-diff',
    method: 'post',
    data: params,
  })
}

// 44获取系统默认佣金
export function getDefaultCommissions(params) {
  return request({
    url: 'systems/get-default-commissions',
    method: 'post',
    data: params,
  })
}

// 45保存系统默认佣金
export function setDefaultCommissions(params) {
  return request({
    url: 'systems/save-default-commissions',
    method: 'post',
    data: params,
  })
}

// 46获取按额度自动降佣金设置
export function getOrderOddsReduce(params) {
  return request({
    url: 'systems/get-order-odds-reduce',
    method: 'post',
    data: params,
  })
}

// 47保存按额度自动降佣金设置
export function setOrderOddsReduce(params) {
  return request({
    url: 'systems/save-order-odds-reduce',
    method: 'post',
    data: params,
  })
}

// 48获取按期数自动降佣金设置
export function getPeriodOddsReduce(params) {
  return request({
    url: 'systems/get-period-odds-reduce',
    method: 'post',
    data: params,
  })
}

// 49保存按期数自动降佣金设置
export function setPeriodOddsReduce(params) {
  return request({
    url: 'systems/save-period-odds-reduce',
    method: 'post',
    data: params,
  })
}

// 50获取网站参数设置
export function getSiteConfigs(params) {
  return request({
    url: 'systems/get-site-configs',
    method: 'post',
    data: params,
  })
}

// 51保存网站参数设置
export function setSiteConfigs(params) {
  return request({
    url: 'systems/save-site-configs',
    method: 'post',
    data: params,
  })
}

// 52获取系统公告设置
export function getBulletins(params) {
  return request({
    url: 'systems/get-bulletins',
    method: 'post',
    data: params,
  })
}

// 53获取系统公告设置
export function setBulletins(params) {
  return request({
    url: 'systems/save-bulletins',
    method: 'post',
    data: params,
  })
}

// 54获取游戏设置
export function getGameSettings(params) {
  return request({
    url: 'systems/get-lottery-settings',
    method: 'post',
    data: params,
  })
}

// 55保存游戏设置
export function setGameSettings(params) {
  return request({
    url: 'systems/save-lottery-settings',
    method: 'post',
    data: params,
  })
}

// 56判断用户是否在开盘日已有投标
export function hasOrder(params) {
  return request({
    url: 'users/has-order',
    method: 'post',
    data: params,
  })
}

// 57获取系统默认盘口
export function getDefaultHandicaps(params) {
  return request({
    url: 'systems/handicaps',
    method: 'post',
    data: params,
  })
}

// 58获取默认游戏分类目录
export function getDefaultGameCategory(params) {
  return request({
    url: 'systems/category',
    method: 'post',
    data: params,
  })
}

// 36获取系统管理-参数  参数为游戏名
export function getGameConfig(params) {
  return request({
    url: 'systems/game-config',
    method: 'post',
    data: params,
  })
}

// 37保存系统管理-参数  参数为游戏名
export function saveGameConfig(params) {
  return request({
    url: 'systems/save-game-config',
    method: 'post',
    data: params,
  })
}

// 38、系统管理-基准赔率-获取基准赔率
export function getBaseOdds(params) {
  return request({
    url: 'systems/base-odds',
    method: 'post',
    data: params,
  })
}

// 39、保存基准赔率
export function saveBaseOdds(params) {
  return request({
    url: 'systems/save-base-odds',
    method: 'post',
    data: params,
  })
}

// 40、系统管理-自动降赔-获取自动降赔设置
export function getOddsReduce(params) {
  return request({
    url: 'systems/get-odds-reduce',
    method: 'post',
    data: params,
  })
}

// 41、系统管理-自动降赔-保存自动降赔设置
export function saveOddsReduce(params) {
  return request({
    url: 'systems/save-odds-reduce',
    method: 'post',
    data: params,
  })
}

// 42、获取系统补货设置
export function getPaddingSettings(params) {
  return request({
    url: 'systems/get-padding-settings',
    method: 'post',
    data: params,
  })
}

// 43、保存系统补货设置
export function savePaddingSettings(params) {
  return request({
    url: 'systems/save-padding-settings',
    method: 'post',
    data: params,
  })
}

// 44、系统管理-退水-获取退水
export function getCommissions(params) {
  return request({
    url: 'systems/commissions',
    method: 'post',
    data: params,
  })
}

// 45、系统管理-退水-保存退水
export function saveCommissions(params) {
  return request({
    url: 'systems/save-commissions',
    method: 'post',
    data: params,
  })
}

// 65、获取彩种列表
export function gameOptionss(params) {
  return request({
    url: 'games/game-options',
    method: 'post',
    data: params,
  })
}

// 46、获取系统配置
export function systemSettings(params) {
  return request({
    url: 'systems/system-settings',
    method: 'post',
    data: params,
  })
}

// 47、保存系统设置
export function updateSystemSettings(params) {
  return request({
    url: 'systems/update-system-settings',
    method: 'post',
    data: params,
  })
}

// 48、恢复信用额度
export function restoreCredit(params) {
  return request({
    url: 'systems/restore-credit',
    method: 'post',
    data: params,
  })
}

// 51、系统管理-限额-获取限额数据
export function getLimits(params) {
  return request({
    url: 'systems/limits',
    method: 'post',
    data: params,
  })
}

// 52、系统管理-限额-保存限额数据
export function saveLimits(params) {
  return request({
    url: 'systems/save-limits',
    method: 'post',
    data: params,
  })
}

// 53、系统管理-降赔-获取降赔数据
export function getOddsReduces(params) {
  return request({
    url: 'systems/odds-reduces',
    method: 'post',
    data: params,
  })
}

// 54、系统管理-降赔-保存降赔数据
export function saveOddsReduces(params) {
  return request({
    url: 'systems/save-odds-reduces',
    method: 'post',
    data: params,
  })
}
