// agent相关接口
import request from '@/utils/request'

// user list
export function listUser(params, page = 1) {
  return request({
    url: `/users/list/${page}`,
    method: 'post',
    data: params,
  })
}

// user 下级数量统计
export function subCount(params) {
  return request({
    url: '/users/sub-count',
    method: 'post',
    data: params,
  })
}

// 验证agent表单
export function validateAgent(params) {
  return request({
    url: '/users/validate-agent-form',
    method: 'post',
    data: params,
  })
}

// 创建新agent
export function createAgent(params) {
  return request({
    url: '/users/create-agent',
    method: 'post',
    data: params,
  })
}

// 验证member表单
export function validateMember(params) {
  return request({
    url: '/users/validate-member-form',
    method: 'post',
    data: params,
  })
}

// 创建新member
export function createMember(params) {
  return request({
    url: '/users/create-member',
    method: 'post',
    data: params,
  })
}

// 编辑用户信息
// 验证agent更新
export function validateUpdateAgent(params) {
  return request({
    url: '/users/validate-update-agent-form',
    method: 'post',
    data: params,
  })
}

// 更新新agent
export function updateAgent(params) {
  return request({
    url: '/users/update-agent',
    method: 'post',
    data: params,
  })
}

// 验证member更新
export function validateUpdateMember(params) {
  return request({
    url: '/users/validate-member-update-form',
    method: 'post',
    data: params,
  })
}

// 更新新member
export function updateMember(params) {
  return request({
    url: '/users/update-member',
    method: 'post',
    data: params,
  })
}

// 更新用户状态
export function changeStatus(params) {
  return request({
    url: '/users/change-status',
    method: 'post',
    data: params,
  })
}

// 用户登录日志
export function listLoginHists(params) {
  return request({
    url: '/loginhists/list',
    method: 'post',
    data: params,
  })
}
// 26、获取当前登录代理的登录记录列表
export function getSelfLog(params) {
  return request({
    url: '/loginhists/self-log',
    method: 'post',
    data: params,
  })
}
// 获取上级占成
export function parentShare(params) {
  return request({
    // url: '/shares/get-upper-agents-share',
    url: '/shares/upper-shares',
    method: 'post',
    data: params,
  })
}

// 提取用户全部额度
export function withdrawAll(params) {
  return request({
    url: '/users/withdraw-all',
    method: 'post',
    data: params,
  })
}

// 验证用户额度
export function validateChangeAmount(params) {
  return request({
    url: '/users/validate-change-amount-form',
    method: 'post',
    data: params,
  })
}

// 变更用户额度
export function changeAmount(params) {
  return request({
    url: '/users/change-amount',
    method: 'post',
    data: params,
  })
}

// 获取上级commissions
export function getDatasCommissions(params) {
  return request({
    url: '/commissions/datas',
    method: 'post',
    data: params,
  })
}

// 保存用户commissions
export function setCommissions(params) {
  return request({
    url: '/commissions/save',
    method: 'post',
    data: params,
  })
}

// 子账号列表
export function listSubAccount(params) {
  return request({
    url: `/users/subaccount-list/${params}`,
    method: 'post',
  })
}

// 验证新增子账号
export function validateAddSubAccount(params) {
  return request({
    url: `/users/validate-subaccount-form`,
    method: 'post',
    data: params,
  })
}

// 新增子账号
export function addSubAccount(params) {
  return request({
    url: `/users/create-sub-account`,
    method: 'post',
    data: params,
  })
}

// 验证更新子账号
export function validateUpdateSubAccount(params) {
  return request({
    url: `/users/validate-update-subaccount-form`,
    method: 'post',
    data: params,
  })
}

// 更新子账号
export function updateSubAccount(params) {
  return request({
    url: `/users/update-subaccount`,
    method: 'post',
    data: params,
  })
}

// 删除子账号
export function deleteSubAccount(params) {
  return request({
    url: `/users/delete-subaccount`,
    method: 'post',
    data: params,
  })
}

// 56是否存在交易行为
export function hasOrder(params) {
  return request({
    url: `/users/has-order`,
    method: 'post',
    data: params,
  })
}
// 23生成验证码
export function getValidCode() {
  return request({
    url: `/sites/gen-valid-code`,
    method: 'post',
  })
}
// 55、获取即时注单
export function getrRealtime(params) {
  return request({
    url: `/orders/realtime`,
    method: 'post',
    data: params,
  })
}
// 55-1、即时注单导航菜单
export function getRealtimePt(params) {
  return request({
    url: `/orders/realtime-pt`,
    method: 'post',
    data: params,
  })
}
// 55-2、即时注单-最新一期信息
export function getRealtimesInfo(params) {
  return request({
    url: `/realtimes/info`,
    method: 'post',
    data: params,
  })
}

// 63、站内消息分页列表
export function getNoticesList(params) {
  if (params) {
    return request({
      url: `/notices/list/` + params,
      method: 'post',
      data: params,
    })
  } else {
    return request({
      url: `/notices/list`,
      method: 'post',
      data: params,
    })
  }
}

// 64-2、删除站内消息（公告）
export function deleteNotice(params) {
  return request({
    url: `/notices/unlink`,
    method: 'post',
    data: params,
  })
}

// 64、添加站内消息（公告）
export function addNotice(params) {
  return request({
    url: `/notices/add`,
    method: 'post',
    data: params,
  })
}

// 64、类别2的即时注单弹框列表 代理系统
export function getMbets(params) {
  return request({
    url: `/orders/mbets`,
    method: 'post',
    data: params,
  })
}

// 39、即时注单页面手动补货
export function setPadding(params) {
  return request({
    url: `/orders/padding`,
    method: 'post',
    data: params,
  })
}

// 41、 报表查询 分类报表
export function getGameReport(params) {
  return request({
    url: `/orders/game-report`,
    method: 'post',
    data: params,
  })
}

// 42、报表查询 交收报表
export function getReport(params) {
  return request({
    url: `/orders/report`,
    method: 'post',
    data: params,
  })
}

// 43、注单明细
export function getOrderBets(params) {
  return request({
    url: `/orders/bets`,
    method: 'post',
    data: params,
  })
}

// 40、获取单一彩种期数
export function getGamePeriods(params) {
  return request({
    url: `/orders/periods`,
    method: 'post',
    data: params,
  })
}

// 30-1、修改子账号状态
export function changeSubaccStatus(params) {
  return request({
    url: `/users/change-subacc-status`,
    method: 'post',
    data: params,
  })
}

// 30-2、修改子账号状态
export function getSubAccInfo(params) {
  return request({
    url: `/users/subacc-info`,
    method: 'post',
    data: params,
  })
}
// 获取用户管理的记录
export function getLogsList(params) {
  return request({
    url: `/logs/list`,
    method: 'post',
    data: params,
  })
}
// 04.1、获取创建子代理时的占成条件
export function getShareConditions(params) {
  return request({
    url: `/users/share-conditions`,
    method: 'post',
    data: params,
  })
}
// 49、返回当前代理是否绑定了二次验证
export function getUseTfa(params) {
  return request({
    url: `/agents/use-tfa`,
    method: 'post',
    data: params,
  })
}
// 50、生成绑定二次验证码链接二维码
export function getGenTfa(params) {
  return request({
    url: `/agents/gen-tfa`,
    method: 'post',
    data: params,
  })
}
// 51、绑定二次验证
export function getSaveTfacode(params) {
  return request({
    url: `/agents/save-tfacode`,
    method: 'post',
    data: params,
  })
}
// 52、解绑二次验证
export function getResetTfa(params) {
  return request({
    url: `/agents/reset-tfa`,
    method: 'post',
    data: params,
  })
}
// 24、获取编辑用户赔率差时所需的自身以及直属父级赔率差设置
export function getOddsdatas(params) {
  return request({
    url: `/odds/datas`,
    method: 'post',
    data: params,
  })
}
// 25、保存用户（代理或者会员）赔率差
export function saveOdds(params) {
  return request({
    url: `/odds/save`,
    method: 'post',
    data: params,
  })
}
