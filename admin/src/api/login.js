// 用户登录相关接口
import request from '@/utils/request'

export function login(params) {
  return request({
    url: '/sites/login',
    method: 'post',
    data: params,
  })
}

export function getUserInfo(params) {
  return request({
    url: '/users/get-user-info',
    method: 'post',
    data: params,
  })
}

export function needChangePwd(params) {
  return request({
    url: '/sites/need-change-pwd',
    method: 'post',
    data: params,
  })
}

export function changePwd(params) {
  return request({
    url: '/sites/change-pwd',
    method: 'post',
    data: params,
  })
}

// export function logout() {
//   return request({
//     url: '/user/logout',
//     method: 'post'
//   })
// }

// 获取图片验证码
export function genValidCode(params) {
  return request({
    url: '/sites/gen-valid-code',
    method: 'post',
    data: params,
  })
}

// 获取主菜单
export function fetchMenus(params) {
  return request({
    url: '/sites/menus',
    method: 'post',
    data: params,
  })
}

// 获取主菜单
export function onlineStat(params) {
  return request({
    url: '/users/online-stat',
    method: 'post',
    data: params,
  })
}
