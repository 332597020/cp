/* Layout */
import Layout from '../views/agent/components/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/

export const agentRouterMap = [
  {
    path: '/login',
    component: () => import('@/views/agent/login/index'),
    hidden: true,
  },
  {
    path: '/changepwd',
    component: () => import('@/views/agent/login/changepwd'),
    hidden: true,
  },
  {
    path: '/noteDetails',
    name: 'noteDetails',
    component: () => import('@/views/agent/zhudan/noteDetails'),
    hidden: true,
    meta: { title: '注单明细', icon: 'form' },
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true,
  },
  {
    path: '/',
    name: '/',
    redirect: '/notice',
  },
  {
    path: '/notice',
    name: 'notice',
    redirect: '/notice/index',
    component: Layout,
    meta: { title: '最新公告', icon: 'form' },
    children: [
      {
        path: 'index',
        name: 'notice_index',
        component: () => import('@/views/agent/notice/index'),
        meta: { title: '最新公告', icon: 'form' },
      },
    ],
  },
  {
    path: '/zhudan',
    name: 'zhudan',
    index: 1, // 与服务器匹配的index值
    redirect: '/zhudan/index',
    component: Layout,
    meta: { title: '即时注单', icon: 'form' },
    children: [
      {
        path: 'index',
        name: 'zhudan_index',
        component: () => import('@/views/agent/zhudan/index'),
        meta: { title: '即时注单', icon: 'form' },
      },
    ],
  },

  // {
  //   path: '/zhudanflow',
  //   name: 'zhudanflow',
  //   index: 2, // 与服务器匹配的index值
  //   redirect: '/zhudanflow/flow',
  //   component: Layout,
  //   meta: { title: '流水注单', icon: 'form' },
  //   children: [
  //     {
  //       path: 'flow',
  //       name: 'zhudan_flow_index',
  //       component: () => import('@/views/agent/zhudan/flow'),
  //       meta: { title: '流水注单', icon: 'form' },
  //     },
  //   ],
  // },

  {
    path: '/buhuo',
    name: 'buhuo',
    index: 3, // 与服务器匹配的index值
    redirect: '/buhuo/index',
    component: Layout,
    meta: { title: '自动补货', icon: 'form' },
    children: [
      {
        path: 'index',
        name: 'buhuo_index',
        component: () => import('@/views/agent/buhuo/index'),
        meta: { title: '自动补货设定', icon: 'form' },
      },
      // {
      //   path: 'setting',
      //   name: 'buhuo_setting',
      //   index: 9, // 与服务器匹配的index值
      //   component: () => import('@/views/agent/buhuo/setting'),
      //   meta: { title: '自动补货设置', icon: 'form' },
      // },
      {
        path: 'log',
        name: 'buhuo_log',
        index: 10, // 与服务器匹配的index值
        component: () => import('@/views/agent/buhuo/log'),
        meta: { title: '自动补货变更记录', icon: 'form' },
      },
    ],
  },

  {
    path: '/user',
    name: 'user',
    index: 4, // 与服务器匹配的index值
    redirect: '/user/list/1/my/my/direct', // 跳转到直属代理
    component: Layout,
    meta: { title: '用户管理', icon: 'example' },
    children: [
      {
        path: 'set/:type/:parent/:opera', // type:1:代理,2:会员,parent:父级名称,opera:add,edit
        name: 'user_add',
        hidden: true,
        component: () => import('@/views/agent/user/set'),
        meta: { title: '新增', icon: 'form' },
      },
      {
        path: 'set/:type/:parent/:opera/:username/:level', // type:1:代理,2:会员,parent:父级名称,opera:add,edit
        name: 'user_update',
        hidden: true,
        component: () => import('@/views/agent/user/set'),
        meta: { title: '修改', icon: 'form' },
      },
      {
        path: 'list/:type/:parent/:plv/:lv', // type:1:代理,2:会员,parent:父级名称,plv:父级level,lv:下级等级,direct:直属,all:全部,1:1级
        name: 'user_list',
        hidden: true,
        component: () => import('@/views/agent/user/list'),
        meta: { title: '列表', icon: 'form' },
      },
      {
        path: 'list/1/my/my/direct',
        name: 'user_1_my_direct',
        index: 11, // 与服务器匹配的index值
        component: () => import('@/views/agent/user/list'),
        meta: { title: '直属代理', icon: 'form' },
      },
      {
        path: 'list/2/my/my/direct',
        name: 'user_2_my_direct',
        component: () => import('@/views/agent/user/list'),
        meta: { title: '直属会员', icon: 'form' },
      },
      {
        path: 'list/1/my/my/all',
        name: 'user_1_my_all',
        component: () => import('@/views/agent/user/list'),
        meta: { title: '全部代理', icon: 'form' },
      },
      {
        path: 'list/2/my/my/all',
        name: 'user_2_my_all',
        component: () => import('@/views/agent/user/list'),
        meta: { title: '全部会员', icon: 'form' },
      },
      {
        path: 'subAccountList',
        name: 'user_sub_account_list',
        index: 12,
        component: () => import('@/views/agent/user/subAccountList'),
        meta: { title: '直属子账号', icon: 'form' },
      },
      {
        path: 'addSubAccountList',
        name: 'user_addSubAccountList',
        hidden: true,
        component: () => import('@/views/agent/user/addSubAccountList'),
        meta: { title: '新增子账号', icon: 'form' },
      },
      {
        // 用户操作记录志
        path: 'dataLog/:username',
        name: 'user_dataLog',
        hidden: true,
        component: () => import('@/views/agent/user/dataLog'),
        meta: { title: '资料变更记录', icon: 'form' },
      },
      {
        // 用户操作记录志
        path: 'record/:username',
        name: 'user_record',
        hidden: true,
        component: () => import('@/views/agent/user/record'),
        meta: { title: '资料变更记录', icon: 'form' },
      },
      {
        // 退水编辑
        path: 'commission/:type/:parent/:opera/:username/:level',
        name: 'user_commission',
        hidden: true,
        component: () => import('@/views/agent/profile/commission'),
        meta: { title: '退水设置', icon: 'form' },
      },
      {
        // 退水编辑
        path: 'odds/:type/:parent/:opera/:username/:level',
        name: 'user_odds',
        hidden: true,
        component: () => import('@/views/agent/user/odds'),
        meta: { title: '赔率差修改', icon: 'form' },
      },
    ],
  },

  {
    path: '/profile',
    name: 'profile',
    index: 5, // 与服务器匹配的index值
    redirect: '/profile/loginlog/my',
    component: Layout,
    meta: { title: '个人管理', icon: 'form' },
    children: [
      // {
      //   path: 'index',
      //   name: 'profile_index',
      //   index: 13,
      //   component: () => import('@/views/agent/profile/index'),
      //   meta: { title: '用户资料', icon: 'form' },
      // },
      {
        // 用户登录日志
        path: 'loginlog/my',
        name: 'profile_loginlog',
        index: 14,
        component: () => import('@/views/agent/profile/loginlog'),
        meta: { title: '登录日志', icon: 'form' },
      },
      {
        // 变更密码
        path: 'changepwd/:username/:cpwd',
        name: 'profile_changepwd',
        index: 15,
        component: () => import('@/views/agent/login/changepwd'),
        meta: { title: '变更密码', icon: 'form' },
      },
      {
        // 二次验证
        path: 'SecVer/:username',
        name: 'profile_SecVer',
        index: 16,
        component: () => import('@/views/agent/profile/SecVer'),
        meta: { title: '二次验证', icon: 'form' },
      },
      // {
      //   // 最新公告
      //   path: 'notice/:username',
      //   name: 'profile_notice',
      //   index: 17,
      //   component: () => import('@/views/agent/notice/index'),
      //   meta: { title: '最新公告', icon: 'form' },
      // },,
    ],
  },

  {
    path: '/report',
    name: 'report',
    index: 6, // 与服务器匹配的index值
    redirect: '/report/index',
    component: Layout,
    meta: { title: '报表查询', icon: 'form' },
    children: [
      {
        path: 'index',
        name: 'report_index',
        index: 18,
        component: () => import('@/views/agent/report/index'),
        meta: { title: '交收分类报表', icon: 'form' },
      },
      {
        path: 'backup',
        name: 'report_backup',
        index: 19,
        component: () => import('@/views/agent/report/backup'),
        meta: { title: '后台更新日志', icon: 'form' },
      },
      {
        path: 'detailedReport', // type判断是交收报表还是分类报表
        name: 'report_detailedReport',
        hidden: true,
        component: () => import('@/views/agent/report/detailedReport'),
        meta: { title: '交收分类报表详情', icon: 'form' },
      },
    ],
  },
  // {
  //   path: '/noticemanager',
  //   name: 'notice_manager',
  //   index: 9, // 与服务器匹配的index值
  //   redirect: '/noticemanager/list',
  //   component: Layout,
  //   meta: { title: '公告管理', icon: 'form' },
  //   children: [
  //     {
  //       path: 'list',
  //       name: 'notice_manager_list',
  //       component: () => import('@/views/agent/notice/list'),
  //       meta: { title: '公告管理', icon: 'form' }
  //     }
  //   ]
  // },

  // {
  //   path: '/buhuolanjie',
  //   name: 'buhuolanjie',
  //   index: 10, // 与服务器匹配的index值
  //   redirect: '/buhuolanjie/index',
  //   component: Layout,
  //   meta: { title: '拦截补货', icon: 'form' },
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'buhuolanjie_index',
  //       component: () => import('@/views/agent/buhuo/index'),
  //       meta: { title: '拦截补货', icon: 'form' }
  //     }
  //   ]
  // },

  // {
  //   path: '/site',
  //   name: 'site',
  //   index: 49, // 与服务器匹配的index值
  //   redirect: '/site/index',
  //   component: Layout,
  //   meta: { title: '公司管理', icon: 'form' },
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'site_index',
  //       component: () => import('@/views/agent/site/index'),
  //       meta: { title: '公司管理', icon: 'form' }
  //     }
  //   ]
  // },

  {
    path: '/sys',
    name: 'sys',
    index: 7, // 与服务器匹配的index值
    redirect: '/sys/parameter',
    component: Layout,
    meta: { title: '系统管理', icon: 'example' },
    children: [
      {
        path: 'parameter',
        name: 'sys_parameter',
        index: 20,
        component: () => import('@/views/agent/sys/parameter'),
        meta: { title: '参数', icon: 'form' },
      },
      {
        path: 'benchmarkOdds',
        name: 'sys_benchmarkOdds',
        index: 21,
        component: () => import('@/views/agent/sys/benchmarkOdds'),
        meta: { title: '基准赔率', icon: 'form' },
      },
      {
        path: 'oddsReduce',
        name: 'sys_oddsReduce',
        index: 22,
        component: () => import('@/views/agent/sys/oddsReduce'),
        meta: { title: '自动降赔', icon: 'form' },
      },
      {
        path: 'paddingSettings',
        name: 'sys_paddingSettings',
        index: 23,
        component: () => import('@/views/agent/sys/paddingSettings'),
        meta: { title: '补货', icon: 'form' },
      },
      {
        path: 'commission',
        name: 'sys_commission',
        index: 24,
        component: () => import('@/views/agent/sys/commission'),
        meta: { title: '退水', icon: 'form' },
      },
      {
        path: 'index',
        name: 'sys_index',
        index: 25,
        component: () => import('@/views/agent/sys/index'),
        meta: { title: '系统', icon: 'form' },
      },
      {
        path: 'limits',
        name: 'sys_limits',
        index: 26,
        component: () => import('@/views/agent/sys/limits'),
        meta: { title: '限额', icon: 'form' },
      },
      {
        path: 'OddsReduces',
        name: 'sys_OddsReduces',
        index: 27,
        component: () => import('@/views/agent/sys/OddsReduces'),
        meta: { title: '降赔', icon: 'form' },
      },
      {
        path: 'notice',
        name: 'sys_notice',
        component: () => import('@/views/agent/sys/notice'),
        meta: { title: '公告设置', icon: 'form' },
      },
      // {
      //   path: 'game',
      //   name: 'sys_game',
      //   component: () => import('@/views/agent/sys/game'),
      //   meta: { title: '彩票设置', icon: 'form' },
      // },
      //
      // {
      //   path: 'odds',
      //   name: 'sys_odds',
      //   index: 22,
      //   component: () => import('@/views/agent/sys/odds'),
      //   meta: { title: '赔率设置', icon: 'form' },
      // },
      // {
      //   path: 'oddsdiff',
      //   name: 'sys_oddsdiff',
      //   index: 23,
      //   component: () => import('@/views/agent/sys/oddsdiff'),
      //   meta: { title: '赔率差', icon: 'form' },
      // },
      // {
      //   path: 'oddsperiod',
      //   name: 'sys_oddsperiod',
      //   index: 24,
      //   component: () => import('@/views/agent/sys/oddsperiod'),
      //   meta: { title: '按期降赔', icon: 'form' },
      // },
      // {
      //   path: 'oddsorder',
      //   name: 'sys_oddsorder',
      //   index: null,
      //   component: () => import('@/views/agent/sys/oddsorder'),
      //   meta: { title: '按注降赔', icon: 'form' },
      // },
      // {
      //   path: 'log',
      //   name: 'sys_log',
      //   index: 26,
      //   component: () => import('@/views/agent/sys/log'),
      //   meta: { title: '系统日志', icon: 'form' },
      // },
      // {
      //   path: 'logined',
      //   name: 'sys_logined',
      //   index: 27,
      //   component: () => import('@/views/agent/sys/logined'),
      //   meta: { title: '登陆统计', icon: 'form' },
      // },
      // {
      //   path: 'online',
      //   name: 'sys_online',
      //   index: 28,
      //   component: () => import('@/views/agent/sys/online'),
      //   meta: { title: '在线统计', icon: 'form' },
      // },
    ],
  },

  {
    path: '/jiang',
    name: 'jiang',
    index: 8, // 与服务器匹配的index值
    redirect: '/jiang/index',
    component: Layout,
    meta: { title: '开奖结果', icon: 'form' },
    children: [
      {
        path: 'index',
        name: 'jiang_index',
        component: () => import('@/views/agent/jiang/index'),
        meta: { title: '开奖结果', icon: 'form' },
      },
    ],
  },

  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'example' },
  //   children: [
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/agent/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree' }
  //     }
  //   ]
  // },

  {
    path: '*',
    redirect: '/404',
    hidden: true,
  },
]

export default agentRouterMap
